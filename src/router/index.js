import Vue from 'vue'
import Router from 'vue-router'
import WallContainer from '@/components/WallContainer'
import MsgWall from '@/components/MsgWall'
import SignIn from '@/components/SignIn'
import Lucky from '@/components/Lucky'
import VueResource from 'vue-resource'

Vue.use(Router)
Vue.use(VueResource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'WallContainer',
      component: WallContainer,
      children: [
        {
          path: 'msgWall',
          name: 'msgWall',
          component: MsgWall,
          meta: {
            keepAlive: true
          }
        },
        {
          path: 'signIn',
          name: 'signIn',
          component: SignIn
        },
        {
          path: 'lucky',
          name: 'lucky',
          component: Lucky
        }
      ]
    }
  ]
})
