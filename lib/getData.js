function getData(socketUrl,dealMsgFn,openFn,closeFn) {
    var connection = new WebSocket(socketUrl);
    connection.onopen = openFn;
    connection.onclose = closeFn;
    connection.onmessage = dealMsgFn;
}

export default getData;