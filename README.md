# chatwall

> A Vue.js project

## 开发说明
直接克隆项目到本地，每次完成一个功能提交修改直接推送到远程，每次推送到远程仓库后在群里同步一下，下一次开发要拉取远程新代码以保证同步，远程代码和本地代码有冲突需要解决冲突后合并，冲突可以自行解决的自行合并，有疑问的和对应的开发人员协商。

大致需要用到的几个git操作，可以重点看，其他的应该不会用到= =：

git clone

git add   git commit  git push

git pull

## Build Setup

``` bash
# install dependencies
npm install

#启动项目
npm start
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 架构说明
主要关注以下几个文件：

index.html  ——可以看作入口的HTML文件，组件在这里渲染

src/App.vue  ——最外层的组件

src/router.js  ——设置路由

src/components  ——存放组件

每个xxx.vue是一个组件，引用组件需要import

组件中```<template>```写HTML结构，```<script>```写数据和逻辑，```<style>```写样式，```<style scoped>```加上scoped可保证此处的样式只被该组件使用。


微信墙：一个组件，需要轮询，不断向后台发送请求，更新数据，每两秒一次，更新显示

签到：一个组件，需要显示已经签到的人（原型中没有，自己加进去），二维码用于扫码签到，每两秒向后台发送一次请求，更新签到列表。

抽奖：一个组件，从已经签到的人中抽取。


可以先实现功能再修改样式（比如那些奇怪的电脑图片可以先用一个容器代替）


发ajax请求：

可以使用jQuery，在```<script>```中```import $ from 'jquery'```就可以使用jquery
```
$.ajax({
type: 'POST',      //请求的方法
url: 'http://localhost:3000/apis/user/signUp',   //请求的地址
contentType: 'application/json',   
data: JSON.stringify({
    'username': username,
    'password': password
}),
success: function (res) {
   //请求成功后的操作
}
})
```

下面的接口设计是暂定的，根据需要修改

## 接口设计：(暂时)
获取微信墙消息：
```
Request Address:
/msg
Request Method:
GET
Request Param: 
null
Response:
(Array)
[{
    "username":(string),
    "advatar":(string),
    "msg":(string)
}]
```
签到：
```
Request Address:
/login
Request Method:
POST
Request Param:
??
Response:
{
    "status":"success"
}
```
抽奖：
```
Request Address:
/getLucky
Request Method:
POST
Request Param:
{
    'label':(string)//奖项名称
    'num':(number)//该奖项获奖人数
}
Response:
{
    'label': (string) //req的label
    'name': (Array) //数组元素是获奖者的名字
}
```

## 代码更新：
- 2017/10/14 cdh
引入lib/getData.js文件，封装了一个websocket连接函数。

使用方法：在需要使用websocket实时获取后台数据更新视图的文件```import getData from (文件路径)```引入模块。

函数参数：第一个参数是连接后台websocket的URL，由后台提供；第二个参数是接收到新数据调用的函数；第三个参数是连接建立时调用的函数（不需要则可省略）；第四个参数是连接关闭时调用的函数（不需要则可省略）；

参考例子：src/components/Lucky.vue
（为了说明如何使用而引入的例子，实际Lucky.vue中不需要使用到websocket的；使用的api是我服务器上自己用来测试暂时写的）

websocket参考资料：烁挺老板的简书：http://www.jianshu.com/p/5314488cdd6e